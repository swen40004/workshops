import java.util.HashSet;

class P extends Thread {
    //the shared synch instance
    protected Synchronise s;

    public P(Synchronise s) {
        this.s = s;
        s.add(this);
    }

    public void run() {
        while (true) {
            task1p();
            s.synch(this);
            task2p();
            s.synch(this);
        }
    }

    private void task1p() {
        System.out.println("1p");
    }

    private void task2p() {
        System.out.println("2p");
    }
}

class Q extends Thread {
    //the shared synch instance
    protected Synchronise s;

    public Q(Synchronise s) {
        this.s = s;
        s.add(this);
    }

    public void run() {
        while (true) {
            task1q();
            s.synch(this);
            task2q();
            s.synch(this);
        }
    }

    private void task1q() {
        System.out.println("1q");
    }

    private void task2q() {
        System.out.println("2q");
    }
}

class Synchronise {
    HashSet<Thread> threads = new HashSet<Thread>();
    HashSet<Thread> toSync = new HashSet<Thread>();

    private void lag() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void synch(Thread t) {
        if (toSync.isEmpty()) {
//            System.out.println("sync start");
            toSync.addAll(threads);
        }
        toSync.remove(t);
        notify();
//        System.out.println("sync " + t.getId());
        try {
            while (!toSync.isEmpty()) {
                wait();
            }
            lag();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void add(Thread t) {
        threads.add(t);
//        System.out.println("added " + t.getId());
    }
}

class UseSynchronise {
    public static void main(String[] args) {
        Synchronise s = new Synchronise();
        Thread p = new P(s);
        Thread q = new Q(s);
        p.start();
        q.start();
    }
}
