import java.util.Random;
import java.util.Arrays;

/**
 * An simple merge sort which only splits the list once, based on Lars
 * Vogels' implementant at
 * http://www.vogella.de/articles/JavaAlgorithmsMergesort/article.html
 */
public class SequentialSort {
    private static final int ARRAY_SIZE = 2000000;

    private static int[] nums;

    public static void main(String args[]) {
        //generate an array of random intergers
        Random rand = new Random();
        nums = new int[ARRAY_SIZE];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = rand.nextInt();
        }

        //sort lower half
        Arrays.sort(nums, 0, nums.length / 2);

        //sort upper half
        Arrays.sort(nums, nums.length / 2, nums.length);

        //merge the two halfs
        merge(nums, 0, nums.length / 2 - 1, nums.length - 1);

//        System.out.println(java.util.Arrays.toString(nums));
    }

    private static void merge(int[] array, int low, int middle, int high) {
        //copy both parts into a helper array
        int[] helper = new int[array.length];
        for (int i = low; i <= high; i++) {
            helper[i] = array[i];
        }

        int i = low;
        int j = middle + 1;
        int k = low;
        //copy the smallest array from either the left or the right side back
        //to the original array
        while (i <= middle && j <= high) {
            if (helper[i] <= helper[j]) {
                array[k] = helper[i];
                i++;
            } else {
                array[k] = helper[j];
                j++;
            }
            k++;
        }

        //copy the rest of the left side of the array into the target array
        while (i <= middle) {
            array[k] = helper[i];
            k++;
            i++;
        }
    }
}
