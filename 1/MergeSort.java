import java.util.Random;

/**
 * An implementation of merge sort, based on Lars Vogels' implementant at
 * http://www.vogella.de/articles/JavaAlgorithmsMergesort/article.html
 */
public class MergeSort {
    private static int ARRAY_SIZE = 2000;

    private int[] array;
    private int low, high;

    public MergeSort(int[] array, int low, int high) {
        this.array = array;
        this.low = low;
        this.high = high;
    }

    private void mergesort() {
        //if low is less than high, the array still needs to be sorted.
        if (low < high) {
            //get the middle of the current array slide
            int middle = (low + high) / 2;

            //sort the lower half of the array
            new MergeSort(array, low, middle).mergesort();

            //sort the upper half of the array
            new MergeSort(array, middle + 1, high).mergesort();

            //merge the two sorted halves
            this.merge(low, middle, high);


        }
    }

    private void merge(int low, int middle, int high) {
        // Copy both parts into a helper array
        int[] helper = new int[array.length];
        for (int i = low; i <= high; i++) {
            helper[i] = array[i];
        }

        int i = low;
        int j = middle + 1;
        int k = low;
        // Copy the smallest array from either the left or the right side back
        // to the original array
        while (i <= middle && j <= high) {
            if (helper[i] <= helper[j]) {
                array[k] = helper[i];
                i++;
            } else {
                array[k] = helper[j];
                j++;
            }
            k++;
        }

        // Copy the rest of the left side of the array into the target array
        while (i <= middle) {
            array[k] = helper[i];
            k++;
            i++;
        }
    }

    public static void main(String[] args) {
        //populate an aray with random integers
        int[] array = new int[ARRAY_SIZE];
        Random rand = new Random();

        // populate the array
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt();
        }

        //sort the array
        new MergeSort(array, 0, array.length - 1).mergesort();

//        System.out.println(java.util.Arrays.toString(array));
    }
}
