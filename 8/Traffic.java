import java.util.ArrayList;

public class Traffic {

    ArrayList<Vehicle> vehicles;

    public Traffic(int size) {
        vehicles = new ArrayList<Vehicle>(size);
        for (int i = 0; i < size; i++) {
            vehicles.add(null);
        }
    }

    public void step(int time) {
        // NOTE: iterate backwards to avoid replacing
        for (int i = vehicles.size() - 1; i >= 0; i--) {
            Vehicle vehicle = vehicles.get(i);
            if (vehicle != null) {
                int vel = vehicle.velocity(time);
                move(i, vel);
            }
        }
    }

    public void render() {
        for (int i = 0; i < vehicles.size(); i++) {
            Vehicle vehicle = vehicles.get(i);
            System.out.printf((vehicle != null) ? "o" : "-");
        }
        System.out.printf("\n");
    }

    public void move(int start, int distance) {
        int end = (start + distance) % vehicles.size();
        if (start != end) {
            vehicles.set(end, vehicles.get(start));
            vehicles.set(start, null);
        }
    }

    public void insert(int index, Vehicle vehicle) {
        vehicles.set(index, vehicle);
    }

}
