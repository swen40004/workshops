public class TrafficEntry {

    public static void main(String[] args) {

        final Traffic traffic = new Traffic(5);
        Vehicle v1 = new Vehicle();
        traffic.insert(0, v1);

        traffic.render();

        Thread trafficThread = new Thread(new Runnable() {
            @Override
            public void run() {
                int time = 0;
                while (true) {
                    traffic.step(time);
                    traffic.render();
                    time++;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Thread.currentThread().interrupt();
                    }
                }
            }
        });
        trafficThread.start();

    }

}
