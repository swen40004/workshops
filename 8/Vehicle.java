public class Vehicle {

    int accel = 1;
    int velMax = 3;

    public int velocity(int time) {
        int vel = accel * time;
        return vel >= velMax ? velMax : vel;
    }

}
